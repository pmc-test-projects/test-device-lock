package com.paulmark.testdevicelock;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    DevicePolicyManager devicePolicyManager;
    ComponentName componentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        devicePolicyManager = DevicePolicyHelper.getDPManager(this);
        componentName = new ComponentName(this, MyAdminReceiver.class);

        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, componentName);
        startActivityForResult(intent, 15);

        Button btnResetPassword = (Button) findViewById(R.id.btnResetPassword);
        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetPassword();
            }
        });

        Button btnSetPassword = (Button) findViewById(R.id.btnSetPassword);
        btnSetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPassword();
            }
        });

        Button btnEnableKeyguard = (Button) findViewById(R.id.btnEnableKeyguard);
        btnEnableKeyguard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lockDevice();
            }
        });
    }

    private void resetPassword() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Toast.makeText(
                    getApplicationContext(),
                    "Changing Password no longer supported",
                    Toast.LENGTH_LONG)
                    .show();
        } else {
            devicePolicyManager.resetPassword(
                    "123456",
                    DevicePolicyManager.RESET_PASSWORD_REQUIRE_ENTRY);

            Toast.makeText(
                    getApplicationContext(),
                    "Password now set to 123456",
                    Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void setPassword() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Toast.makeText(
                    getApplicationContext(),
                    "Changing Password no longer supported",
                    Toast.LENGTH_LONG)
                    .show();
        } else {
            EditText etPassword = (EditText) findViewById(R.id.etPassword);
            String password = etPassword.getText().toString();

            devicePolicyManager.resetPassword(
                    password,
                    DevicePolicyManager.RESET_PASSWORD_REQUIRE_ENTRY);

            Toast.makeText(
                    getApplicationContext(),
                    "Password now set to: " + password,
                    Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void lockDevice() {
        devicePolicyManager.lockNow();
    }
}
