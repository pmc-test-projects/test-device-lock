package com.paulmark.testdevicelock;

import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyBroadcastReceiver extends BroadcastReceiver {

    final boolean isPersistentLock = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        if (keyguardManager.isKeyguardSecure()) {
            if (isPersistentLock) {
                DevicePolicyManager devicePolicyManager = DevicePolicyHelper.getDPManager(context);
                devicePolicyManager.lockNow();
            } else {
                Toast.makeText(
                        context,
                        "Device Unlocked",
                        Toast.LENGTH_LONG)
                        .show();
            }
        }
    }
}
