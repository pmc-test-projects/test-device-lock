package com.paulmark.testdevicelock;

import android.app.admin.DevicePolicyManager;
import android.content.Context;

/**
 * Created by fpage on 20/09/2017.
 */

public class DevicePolicyHelper {

    private static Context context;
    private static DevicePolicyManager devicePolicyManager;

    public static DevicePolicyManager getDPManager(Context aContext) {
        if (context == null) {
            context = aContext;
            devicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        }
        return devicePolicyManager;
    }
}
